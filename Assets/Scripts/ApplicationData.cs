﻿using UnityEngine;
using System.Collections;

public class ApplicationData : MonoBehaviour {

	public static string carNamePlayer1;
	public static string carNamePlayer2;
	public static string levelName;
	public static string gameMode;
	public static KeyCode forwardPlayer1 = KeyCode.W;
	public static KeyCode backwardPlayer1 = KeyCode.S;
	public static KeyCode leftPlayer1 = KeyCode.A;
	public static KeyCode rightPlayer1 = KeyCode.D;
	public static KeyCode forwardPlayer2 = KeyCode.I;
	public static KeyCode backwardPlayer2 = KeyCode.K;
	public static KeyCode leftPlayer2 = KeyCode.J;
	public static KeyCode rightPlayer2 = KeyCode.L;
	public static KeyCode resetCarsKey = KeyCode.Y;
	public static string defeatedPlayerName = "Error!";
	public static int boxCount = 0;

}
